<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input_date</name>
   <tag></tag>
   <elementGuidId>9df4db0e-112f-4774-863d-ecc3560f86f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'txt_visit_date']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>txt_visit_date</value>
      <webElementGuid>f021ca31-9e93-4a8b-83a7-58639534246d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
