<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Program_radio_btn_medicaid</name>
   <tag></tag>
   <elementGuidId>4d5aa6c9-f218-4ba9-bd18-9607dcba8ae3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'radio_program_medicare']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>radio_program_medicare</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>radio_program_medicare</value>
      <webElementGuid>d9cb2838-b491-434e-a972-de3d82d1194e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
