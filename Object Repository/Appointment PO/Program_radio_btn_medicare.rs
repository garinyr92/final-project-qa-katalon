<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Program_radio_btn_medicare</name>
   <tag></tag>
   <elementGuidId>24794f2c-2140-4329-a02c-46c42a049880</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'radio_program_none']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>radio_program_medicare</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>radio_program_none</value>
      <webElementGuid>27356902-b93b-43f2-afb5-f496d1d93357</webElementGuid>
   </webElementProperties>
</WebElementEntity>
