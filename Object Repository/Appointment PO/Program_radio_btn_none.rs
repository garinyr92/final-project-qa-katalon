<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Program_radio_btn_none</name>
   <tag></tag>
   <elementGuidId>140a1a65-27c3-4466-9112-707d69543bb2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'radio_program_medicaid']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>radio_program_medicare</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>radio_program_medicaid</value>
      <webElementGuid>5f3aa07b-74c3-4e7e-b3a7-b6740cb8830d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
