<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Select_facility</name>
   <tag></tag>
   <elementGuidId>63247109-c326-48b6-8072-aaaf0b7aaf3a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'combo_facility']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>combo_facility</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>combo_facility</value>
      <webElementGuid>254e1a35-c6a8-4c22-856e-dcbff0351698</webElementGuid>
   </webElementProperties>
</WebElementEntity>
