<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>readPermission</name>
   <tag></tag>
   <elementGuidId>d579ef7d-ed6b-4323-b346-3cb6267d4e0a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'hospital_readmission']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>hospital_readmission</value>
      <webElementGuid>fe8cb091-8b28-4cae-bd6a-28a42b6b2798</webElementGuid>
   </webElementProperties>
</WebElementEntity>
