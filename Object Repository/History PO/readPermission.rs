<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>readPermission</name>
   <tag></tag>
   <elementGuidId>bdd25fc8-fa2e-4f0f-803b-9af2b78ac253</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'hospital_readmission']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>hospital_readmission</value>
      <webElementGuid>590d78d3-bb77-4b61-8899-a0d2ac4a9c02</webElementGuid>
   </webElementProperties>
</WebElementEntity>
