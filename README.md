# Final Project QA Katalon



## Login Feature
- TC_Login_01 Invalid login
- TC_Login_02 Invalid login
- TC_Login_03 Invalid login
- TC_Login_04 Valid login

## Appointment Feature
- TC_Appointment_01 Valid Appointment

## History Feature
- TC_History_01 Valid History

## How To Run
- For E2E Test (Login -> Appointment -> History), run TC_Login_E2E