import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.keyword.builtin.CallTestCaseKeyword as CallTestCaseKeyword
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import groovy.inspect.swingui.BytecodeCollector as BytecodeCollector
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.By.ById as ById
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebElement as Keys
import org.openqa.selenium.remote.server.handler.FindElement as FindElement
import org.openqa.selenium.support.ui.Select as Select
import org.testng.Assert as Assert

//WebUI.callTestCase(findTestCase('Login Feature/TC_Login_E2E'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.selectOptionByIndex(findTestObject('Object Repository/Appointment PO/Select_facility'), 0)

String facility = 'Tokyo CURA Healthcare Center'

WebUI.click(findTestObject('Object Repository/Appointment PO/chk_readpermission'))

boolean readPermission = WebUI.verifyElementChecked(findTestObject('Object Repository/Appointment PO/chk_readpermission'), 
    0)

WebUI.click(findTestObject('Object Repository/Appointment PO/Program_radio_btn_medicaid'))

String program = WebUI.getAttribute(findTestObject('Object Repository/Appointment PO/Program_radio_btn_medicaid'), 'value')

String date = '25/12/2022'

WebUI.setText(findTestObject('Object Repository/Appointment PO/Input_date'), date)

String comment = 'Test 01'

WebUI.setText(findTestObject('Object Repository/Appointment PO/Input_comment'), comment)

WebUI.click(findTestObject('Object Repository/Appointment PO/btn_submit'))

WebUI.delay(2)

String actualFacility = WebUI.getText(findTestObject('Object Repository/Confirmation PO/facility'))

String actualReadPermission = WebUI.getText(findTestObject('Object Repository/Confirmation PO/readPermission'))

String actualProgram = WebUI.getText(findTestObject('Object Repository/Confirmation PO/program'))

String actualDate = WebUI.getText(findTestObject('Object Repository/Confirmation PO/visit_date'))

String actualComment = WebUI.getText(findTestObject('Object Repository/Confirmation PO/comment'))

permission = null

if (readPermission) {
    permission = 'Yes'
} else {
    permission = 'No'
}

Assert.assertEquals(actualFacility, facility)

Assert.assertEquals(actualReadPermission, permission)

Assert.assertEquals(actualProgram, program)

Assert.assertEquals(actualDate, date)

Assert.assertEquals(actualComment, comment)

WebUI.callTestCase(findTestCase('Test Cases/History Feature/TC_History_01'), [('facility') : facility, ('permission') : permission
        , ('program') : program, ('date') : date, ('comment') : comment], FailureHandling.STOP_ON_FAILURE)

