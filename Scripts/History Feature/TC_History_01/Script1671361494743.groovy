import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable

import org.apache.logging.log4j.core.net.Facility
import org.openqa.selenium.Keys
import org.testng.Assert
import org.testng.Assert as Keys


WebUI.click(findTestObject('Object Repository/Home PO/Toggle_menu'))
WebUI.click(findTestObject('Object Repository/History PO/History_menu'))


String actualTitle = WebUI.getText(findTestObject('Object Repository/History PO/Page_title'))

String actualFacility = WebUI.getText(findTestObject('Object Repository/History PO/facility'))

String actualReadPermission = WebUI.getText(findTestObject('Object Repository/History PO/readPermission'))

String actualProgram = WebUI.getText(findTestObject('Object Repository/History PO/program'))

String actualDate = WebUI.getText(findTestObject('Object Repository/History PO/visit_date'))

String actualComment = WebUI.getText(findTestObject('Object Repository/History PO/comment'))

String expectedTitle = "History";


Assert.assertEquals(actualTitle, expectedTitle)

Assert.assertEquals(actualFacility, facility)

Assert.assertEquals(actualReadPermission, permission)

Assert.assertEquals(actualProgram, program)

Assert.assertEquals(actualDate, date)

Assert.assertEquals(actualComment, comment)