import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys
import org.testng.Assert
import org.testng.Assert as Keys

WebUI.openBrowser(GlobalVariable.baseUrl)

WebUI.click(findTestObject('Object Repository/Home PO/Toggle_menu'))
WebUI.delay(2)
WebUI.click(findTestObject('Object Repository/Home PO/Login_btn'))

String username = WebUI.getAttribute(findTestObject('Object Repository/Login PO/Demo_username'), "value")
String password = "GarinPasswordData";

WebUI.setText(findTestObject('Object Repository/Login PO/Input_username'), username)
WebUI.setText(findTestObject('Object Repository/Login PO/Input_password'), password)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Login PO/Submit_btn'))

String actualAlert = WebUI.getText(findTestObject('Object Repository/Login PO/Text_alert'))

String expectedAlertText = 'Login failed! Please ensure the username and password are valid.'

WebUI.delay(2)

Assert.assertEquals('Validate Error message', expectedAlertText, actualAlert)

WebUI.closeBrowser()